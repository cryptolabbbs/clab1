# Лабораторная работа №1 
## Шифр цезаря и его криптоанализ

Программа шифрует сообщение с помощью шифра цезаря а после пытается найти изначальный сдвиг с помощью частотного анализа  
В программе встроенны 2 примера которые появятся в терминале при запуске программы  

Чтобы запустить программу понадобится Python 3.11.5

```python
python caesar.py
```
